This is a simple test project to compare 2 PHP versions regarding CSV parsing.

Using the `SplFileObject` with UTF-8 tabulated data.

It appears that each line in the sample file are correctly parsed. (both PHP 7 and 8)
