<?php
// phpinfo();

define("CSV_FILE", __DIR__."/data_sample.txt");

$csv = new SplFileObject(CSV_FILE);
$csv->setFlags(SplFileObject::READ_CSV | SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
$csv->setCsvControl("\t", "\"", "\\");

echo "<pre>";
foreach($csv as $line) {
    var_export($line);
}
echo "</pre>";
